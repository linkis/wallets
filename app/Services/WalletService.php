<?php
namespace App\Services;

use App\Models\Currency;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\Sequence;

class WalletService
{
    public function createWalletsForUser(User $user): void
    {
        $currencySequence = [];

        foreach (Currency::all() as $currency) {
            $currencySequence[] = ['currency_id' => $currency->id];
        }

        if (count($currencySequence) > 0) {
            Wallet::factory(count($currencySequence))->state(
                new Sequence(...$currencySequence)
            )->state([
                'user_id' => $user->id,
                'amount' => 0,
            ])->create();
        }
    }

    public function getWalletByUserAndCurrency(Authenticatable $user, Currency $currency): Wallet
    {
        return $user
            ->wallets
            ->where('currency_id', $currency->id)
            ->first();
    }
}
