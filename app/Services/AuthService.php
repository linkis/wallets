<?php
namespace App\Services;

use App\Dto\CreateUserDto;
use App\Dto\LoginUserDto;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthService
{
    public function __construct(
        protected WalletService $walletService
    ) {}

    public function createUser(CreateUserDto $createUserDto): string
    {
        $user = User::create([
            'name' => $createUserDto->name,
            'email' => $createUserDto->email,
            'password' => Hash::make($createUserDto->password)
        ]);

        $this->walletService->createWalletsForUser($user);

        return $this->generateUserToken($user);
    }

    /**
     * @throws ValidationException
     */
    public function loginUser(LoginUserDto $loginUserDto): string
    {
        if (Auth::attempt([
            'email' => $loginUserDto->email,
            'password' => $loginUserDto->password,
        ])) {
            $user = User::where('email', $loginUserDto->email)->first();

            return $this->generateUserToken($user);
        }

        throw ValidationException::withMessages([
            'email' => [__('The provided credentials are incorrect.')],
        ]);
    }

    protected function generateUserToken(User $user): string
    {
        return $user->createToken("User:" . $user->id)->plainTextToken;
    }
}
