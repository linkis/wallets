<?php
namespace App\Services;

use App\Dto\CreateOrderDto;
use App\Enums\OrderStatus;
use App\Models\Order;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class OrderService
{
    public function __construct(
        protected WalletService $walletService
    ) {}

    public function getAllNewOrders(): Builder
    {
        return Order::with(['fromWallet.currency'])->new()->latest();
    }

    public function createOrder(CreateOrderDto $createOrderDto): Order
    {
        $fee = $createOrderDto->price * Order::FEE_PERCENT / 100;
        $priceWithFee = $createOrderDto->price + $fee;

        $walletFrom = Wallet::find($createOrderDto->from_wallet_id);
        $walletTo = Wallet::find($createOrderDto->to_wallet_id);

        if ($walletFrom->user->id !== $walletTo->user->id) {
            throw new AccessDeniedHttpException(__('There must be both wallets of the same user!'));
        }

        if (!$this->isWalletHasCapacity($walletFrom, $createOrderDto->amount)) {
            throw new AccessDeniedHttpException(__('Not enough money on the wallet!'));
        }

        return Order::create([
            'from_wallet_id' => $createOrderDto->from_wallet_id,
            'to_wallet_id' => $createOrderDto->to_wallet_id,
            'amount' => $createOrderDto->amount,
            'price' => $createOrderDto->price,
            'price_with_fee' => $priceWithFee,
            'fee' => $fee,
            'fee_percent' => Order::FEE_PERCENT,
            'status' => OrderStatus::New,
        ]);
    }

    public function deleteOrder(Order $order): void
    {
        if (!Gate::allows('delete-order', $order)) {
            throw new AccessDeniedHttpException(__("Not your order!"));
        }

        $order->delete();
    }

    public function applyOrder(Order $order): Order
    {
        if (!Gate::allows('apply-order', $order)) {
            throw new AccessDeniedHttpException(__("Can't apply to own order!"));
        }

        if ($order->status !== OrderStatus::New) {
            throw new AccessDeniedHttpException(__('Order is not new!'));
        }

        $userBuyer = auth()->user();

        $userBuyerFromWallet = $this->walletService->getWalletByUserAndCurrency(
            $userBuyer,
            $order->toWallet->currency
        );

        $userBuyerToWallet = $this->walletService->getWalletByUserAndCurrency(
            $userBuyer,
            $order->fromWallet->currency
        );

        if (!$this->isWalletHasCapacity($userBuyerFromWallet, $order->price)) {
            throw new AccessDeniedHttpException(__('Not enough money on the wallet!'));
        }

        DB::transaction(function () use ($order, $userBuyerFromWallet, $userBuyerToWallet, $userBuyer) {
            $order->update([
                'status' => OrderStatus::Completed,
                'buyer_user_id' => $userBuyer->id,
            ]);

            $order->fromWallet->update([
                'amount' => $order->fromWallet->amount - $order->amount,
            ]);

            $order->toWallet->update([
                'amount' => $order->toWallet->amount + $order->price,
            ]);

            // TODO: test try-catch and response with braking some request. set amount field as less than 0
            $userBuyerFromWallet->update([
                'amount' => $userBuyerFromWallet->amount - $order->price_with_fee
            ]);

            $userBuyerToWallet->update([
                'amount' => $userBuyerToWallet->amount + $order->amount,
            ]);
        });

        return $order;
    }

    protected function isWalletHasCapacity(Wallet $wallet, float $amount): bool
    {
        $amountOfAllNewOrders = auth()->user()->orders
            ->where('status', OrderStatus::New)
            ->where('from_wallet_id', $wallet->id)
            ->sum('amount');

        return $wallet->amount >= $amountOfAllNewOrders + $amount;
    }

    public function getAllFees(Request $request): Collection
    {
        $query = DB::table('orders')
            ->select(
                'currencies.code as currency',
                DB::raw("SUM(orders.fee) as amount"),
            )
            ->join('wallets', 'orders.to_wallet_id', '=', 'wallets.id')
            ->join('currencies', 'wallets.currency_id', '=', 'currencies.id')
            ->where('orders.status', '=', OrderStatus::Completed)
            ->groupBy('currencies.code');

        try {
            if ($request->get('date_from')) {
                $query->where(
                    'orders.created_at',
                    '>=',
                    Carbon::parse($request->get('date_from'))->startOfDay()->toDateTimeString()
                );
            }

            if ($request->get('date_to')) {
                $query->where(
                    'orders.created_at',
                    '<=',
                    Carbon::parse($request->get('date_to'))->endOfDay()->toDateTimeString()
                );
            }
        } catch (\Exception) {}

        return $query->get();
    }
}
