<?php

namespace App\Http\Requests;

use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateOrderRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'from_wallet_id' => [
                'required',
                Rule::exists('wallets', 'id')->where(function (Builder $query) {
                    return $query->where('user_id', auth()->user()->id);
                }),
            ],
            'to_wallet_id' => [
                'required',
                'different:from_wallet_id',
                Rule::exists('wallets', 'id')->where(function (Builder $query) {
                    return $query->where('user_id', auth()->user()->id);
                }),
            ],
            'amount' => 'required|numeric|min:1|max:500000',
            'price' => 'required|numeric|min:1|max:500000',
        ];
    }
}
