<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetFeesRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'date_from' => 'nullable|date_format:Y-m-d',
            'date_to' => 'nullable|date_format:Y-m-d',
        ];
    }
}
