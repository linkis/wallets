<?php

namespace App\Http\Controllers\Api;

use App\Dto\CreateOrderDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\GetFeesRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    public function __construct(
        protected OrderService $orderService
    ) {}

    public function index(): JsonResource
    {
        $orders = $this->orderService
            ->getAllNewOrders()
            ->paginate();

        return OrderResource::collection($orders);
    }

    public function store(CreateOrderRequest $request): JsonResource
    {
        return OrderResource::make($this->orderService->createOrder(
            CreateOrderDto::fromRequest($request)
        ));
    }

    public function apply(Order $order): JsonResource
    {
        return OrderResource::make($this->orderService->applyOrder($order));
    }

    public function destroy(Order $order): Response
    {
        $this->orderService->deleteOrder($order);

        return response()->noContent();
    }

    public function fee(GetFeesRequest $request): JsonResponse
    {
        return response()->json($this->orderService->getAllFees($request));
    }
}
