<?php

namespace App\Http\Controllers\Api;

use App\Dto\CreateUserDto;
use App\Dto\LoginUserDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\LoginUserRequest;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    public function __construct(
        protected AuthService $authService
    ) {}

    public function createUser(CreateUserRequest $request): JsonResponse
    {
        $token = $this->authService->createUser(
            CreateUserDto::fromRequest($request)
        );

        return response()->json([
            'auth_token' => $token
        ], 201);
    }

    public function loginUser(LoginUserRequest $request): JsonResponse
    {
        $token = $this->authService->loginUser(
            LoginUserDto::fromRequest($request)
        );

        return response()->json([
            'auth_token' => $token
        ]);
    }
}
