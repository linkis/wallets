<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;

class UserWalletResource extends WalletResource
{
    public function toArray(Request $request): array
    {
        return array_merge(
            parent::toArray($request), [
                'amount' => $this->amount,
            ]
        );
    }
}
