<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        /** @var $this Order */
        return [
            'id' => $this->id,
            'from_wallet' => WalletResource::make($this->fromWallet),
            'to_wallet' => WalletResource::make($this->toWallet),
            'buyer_user_id' => $this->buyer->id ?? null,
            'amount' => $this->amount,
            'price' => $this->price,
            'price_with_fee' => $this->price_with_fee,
            'fee' => $this->fee,
            'fee_percent' => $this->fee_percent,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
