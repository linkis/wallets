<?php

namespace App\Models;

use App\Enums\OrderStatus;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int|null $from_wallet_id
 * @property int|null $to_wallet_id
 * @property int|null $buyer_user_id
 * @property float $amount
 * @property float $price
 * @property float $price_with_fee
 * @property float $fee
 * @property int $fee_percent
 * @property OrderStatus $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\User|null $buyer
 * @property-read \App\Models\Wallet|null $fromWallet
 * @property-read \App\Models\Wallet|null $toWallet
 * @method static Builder|Order completed()
 * @method static \Database\Factories\OrderFactory factory($count = null, $state = [])
 * @method static Builder|Order new()
 * @method static Builder|Order newModelQuery()
 * @method static Builder|Order newQuery()
 * @method static Builder|Order onlyTrashed()
 * @method static Builder|Order query()
 * @method static Builder|Order whereAmount($value)
 * @method static Builder|Order whereBuyerUserId($value)
 * @method static Builder|Order whereCreatedAt($value)
 * @method static Builder|Order whereDeletedAt($value)
 * @method static Builder|Order whereFee($value)
 * @method static Builder|Order whereFeePercent($value)
 * @method static Builder|Order whereFromWalletId($value)
 * @method static Builder|Order whereId($value)
 * @method static Builder|Order wherePrice($value)
 * @method static Builder|Order wherePriceWithFee($value)
 * @method static Builder|Order whereStatus($value)
 * @method static Builder|Order whereToWalletId($value)
 * @method static Builder|Order whereUpdatedAt($value)
 * @method static Builder|Order withTrashed()
 * @method static Builder|Order withoutTrashed()
 * @mixin Eloquent
 */
class Order extends Model
{
    use HasFactory, SoftDeletes;

    public const FEE_PERCENT = 2;

    // TODO: add uuid field and change with id in routes
    protected $fillable = [
        'from_wallet_id',
        'to_wallet_id',
        'buyer_user_id',
        'amount',
        'price',
        'price_with_fee',
        'fee',
        'fee_percent',
        'status',
    ];

    protected $casts = [
        'status' => OrderStatus::class,
        'amount' => 'float',
        'price' => 'float',
        'price_with_fee' => 'float',
        'fee' => 'float',
        'fee_percent' => 'integer',
    ];

    public function fromWallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class, 'from_wallet_id');
    }

    public function toWallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class, 'to_wallet_id');
    }

    public function buyer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'buyer_user_id');
    }

    public function scopeNew(Builder $query): void
    {
        $query->where('status', OrderStatus::New);
    }

    public function scopeCompleted(Builder $query): void
    {
        $query->where('status', OrderStatus::Completed);
    }
}
