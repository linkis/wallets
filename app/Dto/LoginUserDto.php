<?php
namespace App\Dto;

use App\Http\Requests\LoginUserRequest;

readonly class LoginUserDto
{
    public function __construct(
        public string $email,
        public string $password,
    ) {}

    public static function fromRequest(LoginUserRequest $request): LoginUserDto
    {
        return new self(
            email: $request->validated('email'),
            password: $request->validated('password'),
        );
    }
}
