<?php
namespace App\Dto;

use App\Http\Requests\CreateOrderRequest;

readonly class CreateOrderDto
{
    public function __construct(
        public int $from_wallet_id,
        public int $to_wallet_id,
        public float $amount,
        public float $price,
    ) {}

    public static function fromRequest(CreateOrderRequest $request): CreateOrderDto
    {
        return new self(
            from_wallet_id: $request->validated('from_wallet_id'),
            to_wallet_id: $request->validated('to_wallet_id'),
            amount: $request->validated('amount'),
            price: $request->validated('price'),
        );
    }
}
