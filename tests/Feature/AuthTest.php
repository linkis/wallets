<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function test_register_returns_successful_response(): void
    {
        $response = $this->postJson('/api/auth/register', [
            'name' => 'New Test User',
            'email' => 'new_test_user@example.com',
            'password' => 'password',
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'auth_token',
            ]);
    }

    public function test_register_returns_validation_errors(): void
    {
        $this->postJson('/api/auth/register', [
            'name' => 'New Test User',
            'email' => 'new_test_user@example.com',
            'password' => 'password',
        ]);

        $response1 = $this->postJson('/api/auth/register', [
            'name' => 'New Test User',
            'email' => 'new_test_user@example.com',
            'password' => 'password',
        ]);

        $response2 = $this->postJson('/api/auth/register');

        $response1
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors',
            ])
            ->assertValid([
                'name',
                'password'
            ])
            ->assertInvalid([
                'email' => __('The email has already been taken.')
            ]);

        $response2
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors',
            ])
            ->assertInvalid([
                'name' => __('The name field is required.'),
                'email' => __('The email field is required.'),
                'password' => __('The password field is required.'),
            ]);
    }

    public function test_login_returns_successful_response()
    {
        $this->postJson('/api/auth/register', [
            'name' => 'New Test User',
            'email' => 'new_test_user@example.com',
            'password' => 'password',
        ]);

        $response = $this->postJson('/api/auth/login', [
            'email' => 'new_test_user@example.com',
            'password' => 'password',
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'auth_token',
            ]);
    }

    public function test_login_returns_validation_errors(): void
    {
        $response = $this->postJson('/api/auth/login', [
            'email' => 'new_test_user@example.com',
            'password' => 'password',
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors',
            ])
            ->assertInvalid([
                'email' => __('The provided credentials are incorrect.'),
            ]);

        $response2 = $this->postJson('/api/auth/login');

        $response2
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors',
            ])
            ->assertInvalid([
                'email' => __('The email field is required.'),
                'password' => __('The password field is required.'),
            ]);
    }
}
