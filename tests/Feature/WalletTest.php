<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class WalletTest extends TestCase
{
    use RefreshDatabase;

    public function test_wallets_response_successful(): void
    {
        User::factory()
            ->has(Wallet::factory(10))
            ->create();

        $this->actingAs(
            User::factory()
                ->has(Wallet::factory(4))
                ->create()
        );

        $response = $this->getJson('/api/wallets');

        $response
            ->assertStatus(200)
            ->assertJsonCount(4, 'data')
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'currency',
                        'amount',
                    ]
                ],
            ]);
    }
}
