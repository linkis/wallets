## Wallets app

### Requirements

- PHP 8
- Docker

### Installation
````
git clone git@gitlab.com:linkis/wallets.git
cd wallets

cp .env.example .env
composer install

./vendor/bin/sail up -d

./vendor/bin/sail artisan migrate --seed
````

You can use prepared Postman collection `Wallets.postman_collection.json` in root of project.

Don't forget about `Accept: application/json` header.
