<?php

namespace Database\Factories;

use App\Enums\OrderStatus;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $feePercent = fake()->numberBetween(1, 10);
        $price = fake()->randomFloat(null, 40, 2000);
        $fee = $price * $feePercent / 100;
        $priceWithFee = $price + $fee;
        $status = fake()->randomElement(OrderStatus::cases());

        $firstUserId = DB::table('users')
            ->inRandomOrder()
            ->pluck('id')
            ->first();

        $secondUserId = DB::table('users')
            ->where('id', '!=', $firstUserId)
            ->inRandomOrder()
            ->pluck('id')
            ->first();

        $firstUserWallets = DB::table('wallets')
            ->where('user_id', $firstUserId)
            ->inRandomOrder()
            ->pluck('id');

        return [
            'from_wallet_id' => $firstUserWallets->first(),
            'to_wallet_id' => $firstUserWallets->last(),
            'buyer_user_id' => $status === OrderStatus::Completed ? $secondUserId : null,
            'amount' => fake()->randomFloat(null, 40, 2000),
            'price' => $price,
            'price_with_fee' => $priceWithFee,
            'fee' => $fee,
            'fee_percent' => $feePercent,
            'status' => $status
        ];
    }
}
