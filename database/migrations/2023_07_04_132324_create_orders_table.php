<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->foreignId('from_wallet_id')
                ->nullable()
                ->constrained('wallets')
                ->restrictOnUpdate()
                ->nullOnDelete();
            $table->foreignId('to_wallet_id')
                ->nullable()
                ->constrained('wallets')
                ->restrictOnUpdate()
                ->nullOnDelete();
            $table->foreignId('buyer_user_id')
                ->nullable()
                ->constrained('users')
                ->restrictOnUpdate()
                ->nullOnDelete();

            $table->unsignedDecimal('amount', 19, 4);
            $table->unsignedDecimal('price', 19, 4);
            $table->unsignedDecimal('price_with_fee', 19, 4);
            $table->unsignedDecimal('fee', 19, 4);
            $table->unsignedTinyInteger('fee_percent');
            $table->string('status', 20);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
