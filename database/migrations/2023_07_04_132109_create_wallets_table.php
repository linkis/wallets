<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')
                ->nullable()
                ->constrained()
                ->restrictOnUpdate()
                ->nullOnDelete();
            $table->foreignId('currency_id')
                ->nullable()
                ->constrained()
                ->restrictOnUpdate()
                ->nullOnDelete();

            $table->unsignedDecimal('amount', 19, 4);

            $table->timestamps();

            $table->unique(['user_id', 'currency_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('wallets');
    }
};
