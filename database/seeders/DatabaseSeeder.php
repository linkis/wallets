<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Currency;
use App\Models\Order;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('currencies')->truncate();

        $currencyCodes = ['UAH', 'USD', 'EUR'];
        $currencySequence = [];

        foreach ($currencyCodes as $code) {
            $currency = Currency::factory()->create([
                'code' => $code
            ]);

            $currencySequence[] = ['currency_id' => $currency->id];
        }

        User::factory(100)
            ->has(
                Wallet::factory(count($currencySequence))->state(
                    new Sequence(...$currencySequence)
                )
            )
            ->create();

        Order::factory(1000)->create();
    }
}
