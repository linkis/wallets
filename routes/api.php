<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Resources\UserWalletResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/auth/register', [AuthController::class, 'createUser']);
Route::post('/auth/login', [AuthController::class, 'loginUser']);

Route::group([
    'middleware' => 'auth:sanctum'
], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('/wallets', function (Request $request) {
        return UserWalletResource::collection($request->user()->wallets);
    });

    Route::apiResource('/orders', OrderController::class)->except(['update', 'show']);
    Route::post('/orders/{order}/apply', [OrderController::class, 'apply']);

    Route::get('/orders/fee', [OrderController::class, 'fee']);
});


